Ext.define('gpp.model.GPPRemoteModel', {
	extend: 'Ext.data.Model',
	
	config: {
		fields: [
			{ name: 'id', type: 'string' },
			{ name: 'address', type: 'string' },
			{ name: 'rate', type: 'string' }, 
			{ name: 'lat', type: 'string' }, 
			{ name: 'lng', type: 'string' },
			{ name: 'payment_methods', convert: paymentMethods },
			{ name: 'lot_type', mapping: 'carpark_type_str', type: 'string'},
			{ name: 'payment_options', type: 'string' },
			{ name: 'capacity', type: 'string' },
			{ name: 'max_height', type: 'string', convert: maxHeight },
			{ name: 'rate_details', mapping: 'rate_details.periods', type: 'array', convert: rateDetails }
		],
		
		idProperty: 'id'
	}
});

function paymentMethods(value, record) {
	return value.join(' | ');
}

function maxHeight(value, record) {
	if (value == 0) {
		return ' No height restriction';
	} else {
		return value;
	}
}

function rateDetails(value, record) {
	//console.dir(value);
	var result = "";
	for (var i = 0; i < value.length; i++) {
		result += '<div><strong>' + value[i].title + '</strong></div>';
		
		var rates = value[i].rates;
		for (var j = 0; j < rates.length; j++) {
			result += rates[j].when + ': ' + rates[j].rate + '<br />';
		}
	}
	return result;
}