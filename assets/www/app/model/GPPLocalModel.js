Ext.define('gpp.model.GPPLocalModel', {
	extend: 'Ext.data.Model',
	
	config: {
		fields: [
			{ name: 'id', type: 'string' },
			{ name: 'address', type: 'string' },
			{ name: 'rate', type: 'string' }, 
			{ name: 'lat', type: 'string' }, 
			{ name: 'lng', type: 'string' },
			{ name: 'payment_methods', type: 'string' },
			{ name: 'lot_type', mapping: 'carpark_type_str', type: 'string'},
			{ name: 'payment_options', type: 'string' },
			{ name: 'capacity', type: 'string' },
			{ name: 'max_height', type: 'string' },
			{ name: 'rate_details', mapping: 'rate_details.periods', type: 'string' },
			{ name: 'distance', type: 'integer' }, 
			{ name: 'distanceStr', type: 'string' } 
		],
		
		idProperty: 'id',
		identifier: 'uuid',
		
		proxy: {
			type: 'localstorage',
			id: 'gpp'
		}
	}
});