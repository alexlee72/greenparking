Ext.define('gpp.store.GPPLocalStore', {
	extend: 'Ext.data.Store',
	
	config: {
		autoLoad: false,
		model: 'gpp.model.GPPLocalModel',
		storeId: 'localStore',
		sorters: [
			{
				property: 'distance',
				direction: 'ASC'
			}
		]
	}
})