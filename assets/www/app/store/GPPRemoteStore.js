Ext.define('gpp.store.GPPRemoteStore', {
	extend: 'Ext.data.Store',
	
	config: {
		autoLoad: false,
		model: 'gpp.model.GPPRemoteModel',
		storeId: 'remoteStore',
		
		proxy: {
			timeout: 30000,
			type: 'ajax',
            url: 'http://www1.toronto.ca/City_Of_Toronto/Information_&_Technology/Open_Data/Data_Sets/Assets/Files/greenPParking.json',
			reader: {
				type: 'json',
				rootProperty: 'carparks'
			}
		}
	}
})