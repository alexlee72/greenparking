var _isEnd = false;
var _numberPerPage = 10;
var _totalCount = -1;
var _selectedMarkerIndex = -1;
var _pageNumber = 1;

Ext.define('gpp.controller.GPPMapController', {
	extend: 'Ext.app.Controller',
	xtype: 'gppmapcontroller',
	
	requires: [
		'Ext.device.Geolocation'
	],
	
	config: {
		refs: {
			map: 'gppmapview',
			loadMoreButton: 'button[action=loadmore]',
			refreshButton: 'button[action=refresh]',
			previousButton: 'button[action=previous]',
			nextButton: 'button[action=next]'
		},
		control: {
			map: {
				maprender: 'showmap'
			},
			loadMoreButton: {
				tap: 'loadMore'
			},
			refreshButton: {
				tap: 'refresh'
			},
			previousButton: {
				tap: 'goToPrevious'
			},
			nextButton: {
				tap: 'goToNext'
			} 
		}
	},
	
	init: function() {
		var localStore = Ext.getStore('localStore');
		localStore.load();
		
		if (localStore.getCount() == 0) {
			var remoteStore = Ext.getStore('remoteStore');
			remoteStore.load({
				callback: function(records, operation, success) {
					var len = records.length;
					for (var i = 0; i < len; i++) {
						localStore.add(records[i]._data);						
					}
					localStore.sync();
					
					this.getUserLocation();
				},
				
				scope: this
			}); // end of load
		} else {
			this.getUserLocation();
		}// end of if
	}, // end of init
	
	//*************************************************************************
	// Event handlers
	//*************************************************************************
	showmap: function(component, map, eOpts) {
		component.isOpenInfo = false;
		component.markers = [];
		
		this.watchUserPosition(false);
		var self = this;
		
		google.maps.event.addListener(map, 'click', function() {
			if (component.isOpenInfo === true) {
				component.infoBox.close();
				component.isOpenInfo = false;
				_selectedMarkerIndex = -1;
				self.getPreviousButton().disable();
				self.getNextButton().disable();
			}
		})		
	}, // end of showmap
	
	refresh: function (button, e, eOpts) {
		console.log('refresh');
		_isEnd = false;
		_pageNumber = 1;
		this.getUserLocation();
	}, // end of refresh
	
	loadMore: function (button, e, eOpts) {
		console.log('loadMore');
		if (_isEnd === true) {
			console.log('No more to load - 1');
			return;
		}
		if (_pageNumber * _numberPerPage > _totalCount) {
			_isEnd = true;
			console.log('No more to load - 2');
			return;
		}
		
		_pageNumber += 1;
		console.log('loading more: page number: ' + _pageNumber);
		
		this.showMarkers(_pageNumber);
	}, // end of loadMore
	
	goToPrevious: function() {
		if (_selectedMarkerIndex == -1) {
			return;
		}
		if (_selectedMarkerIndex == 0) {
			return;
		} else {
			_selectedMarkerIndex--; 	
		}
		google.maps.event.trigger(this.getMap().markers[_selectedMarkerIndex], 'mouseup');
	}, // end of previous
	
	goToNext: function() {
		if (_selectedMarkerIndex == -1) {
			return;
		}
		if (_selectedMarkerIndex == this.getMap().markers.length - 1) {
			return;
		} else {
			_selectedMarkerIndex++; 	
		}
		google.maps.event.trigger(this.getMap().markers[_selectedMarkerIndex], 'mouseup');
	}, // end of next
	
	//*************************************************************************
	// Private functions
	//*************************************************************************
	
	watchUserPosition: function(isWatch) {
		var comp = this.getMap();
		Ext.device.Geolocation.watchPosition({
			frequency: 5000,
			allowHighAccuracy: true,
			callback: function(position) {
				if (isWatch) {
					comp.setMapCenter(position.coords);
				}
				
				if (comp.userMarker == undefined || comp.userMarker == null) {
					comp.userMarker = new google.maps.Marker({
						position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
						map: comp.getMap()
					});
					
					comp.setMapCenter(position.coords);
				}
				comp.user = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				comp.userMarker.setPosition(comp.user);
			}, // end of callback
			failure: function() {
				console.log('*****ERROR: Failed to watch user location.');
			}
		});
	}, // end of watchUserPosition
	
	getUserLocation: function() {
		Ext.device.Geolocation.getCurrentPosition({
			success: function(position) {
				this.getMap().setMapCenter(position.coords);
				this.setDistance(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
			},
			failure: function() {
				console.log('******ERROR: Failed to get current position.');
			},
			scope: this
		});
	}, // end of getUserLocation
	
	setDistance: function(fromPosition) {
		var localStore = Ext.getStore('localStore');
		localStore.clearFilter();
		
		localStore.each(function(carpark, index, length) {
			var toPosition = new google.maps.LatLng(carpark.get('lat'), carpark.get('lng'));
			var distance = Math.round(google.maps.geometry.spherical.computeDistanceBetween(fromPosition, toPosition));
			var distanceStr = null;
			if (distance < 1000) {
				distanceStr = distance.toString() + 'm';
			} else {
				distanceStr = Number(distance / 1000).toFixed(1) + 'Km';
			}

			carpark.set('distance', distance);
			carpark.set('distanceStr', distanceStr);
		});
		
		localStore.sync();
		
		this.showMarkers(1);			
	}, // end of setDistance
	
	showMarkers: function(pageNumber) {
		var self = this;
		var comp = this.getMap();
		var len = comp.markers.length;
		var markerBounds = new google.maps.LatLngBounds();
		
		for (var i = 0; i < len; i++) {
			comp.markers[i].setMap(null);
		}
		
		var parkingStore = Ext.getStore('localStore');
		comp.markers.length = 0;
		parkingStore.clearFilter();
		
		_totalCount = parkingStore.data.length;
		
		var maxDistance = 1000000;
		if (_totalCount > pageNumber * _numberPerPage ) {
			maxDistance = parkingStore.data.items[pageNumber * _numberPerPage].get('distance');
		} 
		
		parkingStore.filterBy(function(record) {
			if (record.get('distance') < maxDistance) {
				return true;
			} else {
				return false;
			}
		});
		
		parkingStore.each(function(carpark, index, length) {
			carpark.data.xindex = index + 1;

			var position = new google.maps.LatLng(carpark.get('lat'), carpark.get('lng'));
			markerBounds.extend(position);
			
			var parkingMarker = new google.maps.Marker({
				position: position,
				map: comp.getMap(),
				carpark: carpark,
				icon: "resources/images/icon-pink-ball-32.png"
			});
			
			parkingMarker.setClickable(true);
			comp.markers.push(parkingMarker);
			
			google.maps.event.addListener(parkingMarker, 'mouseup', function() {
				comp.isOpenInfo = true;
				_selectedMarkerIndex = comp.markers.indexOf(parkingMarker);
				self.getPreviousButton().enable();
				self.getNextButton().enable();
				
				var park = parkingMarker.carpark;
				var position = new google.maps.LatLng(park.get('lat'), park.get('lng'));
				
				var boxText = document.createElement('div');
				boxText.id = park.get('id');
				boxText.class = 'callout';
				boxText.style.cssText = 'border: 0px solid black; margin-top: 0px; padding: 5px; color: white; font-size: 11px';
				boxText.innerHTML = '<div>[' + park.get('xindex') + ']' + park.get('address') + '</div><div>' + park.get('distanceStr') + '</div>';
				
				var myOptions = {
					content: boxText,
					disableAutoPan: false,
					maxWidth: 0,
					pixelOffset: new google.maps.Size(-107, -80),
					zIndex: null,
					boxStyle: {
						background: "url('resources/images/bg-callout.png') no-repeat",
						opacity: 0.85,
						width: "195px",
						height: "50px"
					},
					closeBoxMargin: "0px 0px 2px 2px",
					closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
					infoBoxClearance: new google.maps.Size(1, 1),
					isHidden: false,
					pane: "floatPane",
					enableEventPropagation: false
				};
				
				if (comp.infoBox == undefined || comp.infoBox == null) {
					comp.infoBox = new InfoBox();
					
					google.maps.event.addListener(comp.infoBox, 'closeclick', function() {
						if (comp.isOpenInfo === true) {
							_selectedMarkerIndex = -1;
							self.getPreviousButton().disable();
							self.getNextButton().disable();
							comp.isOpenInfo = !comp.isOpenInfo;
						}
					});
				}
				
				comp.infoBox.setOptions(myOptions);
				comp.infoBox.open(parkingMarker.map, this);
				comp.setMapCenter(position);
			}); // end of addListener			
		}); // end of each
		
		this.getMap().getMap().fitBounds(markerBounds);
		
	} // end of showMarkers
})