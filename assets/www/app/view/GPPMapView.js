Ext.define('gpp.view.GPPMapView', {
	extend: 'Ext.Map',
	xtype: 'gppmapview',
	
	config: {
		items: [
			{
				xtype: 'toolbar',
				docked: 'top',
				items: [
					{
						xtype: 'textfield',
						name: 'search',
						itemId: 'searchField'
					},
					{
						xtype: 'button',
						action: 'cancel',
						text: 'Cancel',
						placeHolder: 'Enter address, postal code etc',
						margin: '0 0 0 30'
					}
				]
			},
			{
				xtype: 'button',
				action: 'refresh',
				right: '20px',
				bottom: '70px',
				iconCls: 'refresh',
				iconMask: true,
			},
			{
				xtype: 'button',
				action: 'loadmore',
				right: '20px',
				bottom: '20px',
				iconCls: 'download',
				iconMask: true,
			},
			{
				xtype: 'button',
				action: 'previous',
				left: '20px',
				bottom: '20px',
				iconCls: 'arrow_left',
				iconMask: true,
				disabled: true
			},
			{
				xtype: 'button',
				action: 'next',
				left: '70px',
				bottom: '20px',
				iconCls: 'arrow_right',
				iconMask: true,
				disabled: true
			}
		]

	}
});