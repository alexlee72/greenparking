Ext.define('gpp.view.GPPSettingsView', {
	extend: 'Ext.Container',
	xtype: 'gppsettingsview',
	
	config: {
        html: 'Settings View'
	}
});