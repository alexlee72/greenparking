Ext.define('gpp.view.GPPMainView', {
    extend: 'Ext.tab.Panel',
    xtype: 'gppmainview',


    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Map',
                iconCls: 'maps',
				xtype: 'gppmapview'
            },
            {
                title: 'List',
                iconCls: 'organize',
				xtype: 'gpplistview'
            },
            {
                title: 'Timer',
                iconCls: 'time',
				xtype: 'gpptimerview'
            },
            {
                title: 'Settings',
                iconCls: 'settings',
				xtype: 'gppsettingsview'
            }
        ]
    }
});
