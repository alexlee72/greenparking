Ext.define('gpp.view.GPPListView', {
	extend: 'Ext.dataview.List',
	xtype: 'gpplistview',
	
	config: {
		store: 'localStore',
		itemTpl: '<div><b>{xindex}.</b> {address}</div> <div>{rate}</div> <div>Distance: {distanceStr}</div>',
		onItemDisclosure: true
	}
});